var EvolucionDiferencial = function(){
    
    this.numeroIndividuosPorPoblacion = 0;
    this.numeroPoblaciones = 0;
    this.crossRate = 0;
    this.mutationRate = 0;
    this.restricciones = null;
    this.individuos = null;
    this.reparaciones = 0;
    this.posicionesProtegidas = 0;
    
    this.getIndividuos = function(){
        return this.individuos.slice(0);
    }
    
    this.setRestricciones = function(restricciones){
        this.restricciones = restricciones;
    }
    
    this.inicializaEvolucionDiferencial = function(poblaciones, individuos, cr, mr, restricciones, posicionesProtegidas){
        this.numeroIndividuosPorPoblacion = individuos;
        this.numeroPoblaciones = poblaciones;
        this.crossRate = cr;
        this.mutationRate = mr;
        this.restricciones = restricciones;
        this.posicionesProtegidas = posicionesProtegidas;
    }
    
    this.getMejorIndividuo = function(){   
        if(!this.individuos) this.individuos = new Array();
        var numeroIndividuoActual = 0;
        var individuoActual = null;
        while (numeroIndividuoActual < this.numeroIndividuosPorPoblacion) {
            individuoActual = this.generaIndividuoAleatorio(this.individuos[numeroIndividuoActual]);
            individuoActual.setAptitud(this.calculaAptitudDeIndividuo(individuoActual));
            this.individuos[numeroIndividuoActual] = individuoActual;
            numeroIndividuoActual++;
        }
        //console.log("POBLACION:"+this.individuos);
        var mejorIndividuo = this.getMejorIndividuoDePoblacion();
        console.log("POBLACION INICIAL GENERADA");
        console.log("Mejor Individuo: "+mejorIndividuo.getGenotipo());
        console.log("Aptitud: "+mejorIndividuo.getAptitud());
        return mejorIndividuo.getGenotipo().slice();
        
        //ahora ejecutamos toda la lógica del evolutivo
        var numeroDePoblacionConvergencia = 0;
        var numeroPoblacion = 1;
        while( numeroPoblacion < this.numeroPoblaciones ){
            
            numeroIndividuoActual = 0;
            while(numeroIndividuoActual < this.numeroIndividuosPorPoblacion){
                individuoActual = this.generaTrial(numeroIndividuoActual);
                
                if(individuoActual.getAptitud() < this.individuos[numeroIndividuoActual].getAptitud()){
                    //como el trial es menor, entonces el se quedará en la población actual
                    this.individuos[numeroIndividuoActual] = individuoActual;
                }
                
                numeroIndividuoActual++;
            }
            mejorIndividuo = this.getMejorIndividuoDePoblacion();
            if(mejorIndividuo.getAptitud() == 0 && numeroDePoblacionConvergencia == 0){
                numeroDePoblacionConvergencia = numeroPoblacion;
            }
            
            numeroPoblacion++;
        }
        
        mejorIndividuo = this.getMejorIndividuoDePoblacion();
        console.log("EVOLUCIÓN DIFERENCIAL FINALIZADA");
        console.log("Mejor Individuo: "+mejorIndividuo.getGenotipo());
        console.log("Aptitud: "+mejorIndividuo.getAptitud());
        console.log("Número de Población de Convergencia: "+numeroDePoblacionConvergencia);
        //console.log("Reparaciones: "+this.reparaciones);
        
        return mejorIndividuo.getGenotipo().slice();
    }
    
    this.generaTrial = function(posicionBase){
        var vectorBase = this.individuos[posicionBase].getGenotipo();
        
        //calculamos las posiciones aleatorias de los otros tres vectores
        var posicionesIndividuosAleatorios = new Array();
        var numeroIndividuosAleatorios = 0;
        var posicionAleatoria = posicionBase;
        var posicionAleatoriaYaExista = true;
        while(numeroIndividuosAleatorios < 3){
            posicionAleatoriaYaExista = true;
            while(posicionAleatoria == posicionBase || posicionAleatoriaYaExista){
                posicionAleatoria = parseInt(this.getNumeroAleatorioEnRango(0, this.numeroIndividuosPorPoblacion-1));
                posicionAleatoriaYaExista = false;
                for(var i=0; i<posicionesIndividuosAleatorios.length; i++){
                    if(posicionesIndividuosAleatorios[i] == posicionAleatoria){
                        posicionAleatoriaYaExista = true;
                        break;
                    }   
                }
            }
            
            //si llegó aqui quiere decir que es una posición distinta de las anteriores
            if(!posicionAleatoriaYaExista){
                posicionesIndividuosAleatorios.push(posicionAleatoria);
                numeroIndividuosAleatorios++;   
            }
        }
        //console.log("posicion elementos aleatorios: "+posicionesIndividuosAleatorios);
        
        //generamos el vector ruidoso
        var a = this.individuos[posicionesIndividuosAleatorios[0]].getGenotipo();
        var b = this.individuos[posicionesIndividuosAleatorios[1]].getGenotipo();
        var c = this.individuos[posicionesIndividuosAleatorios[2]].getGenotipo();
        
        //console.log("a: "+a);
        //console.log("b: "+b);
        //console.log("c: "+c);
        var vectorResta = this.getRestaDeVectores(a,b);
        //console.log("resta vectores: "+vectorResta);
        var vectorFactor = this.getMultiplicacionDeVectorPorEscalar(vectorResta.slice(0), this.mutationRate);
        //console.log("vector factor: "+vectorFactor);
        var vectorRuidoso = this.getSumaDeVectores(c, vectorFactor);
        
        //console.log("vectorRuidoso: "+vectorRuidoso);
        
        //ahora que ya tenemos el vector ruidoso, entonces hacemos la cruza
        var trial = vectorBase.slice(0);
        var probabilidadAleatoria = 0.0;
        //console.log("longitud vector base: "+vectorBase.length);
        //console.log("base: "+vectorBase);
        for(var k=this.posicionesProtegidas; k<vectorBase.length; k++){
            probabilidadAleatoria = Math.random();
            //console.log("probabilidad: "+probabilidadAleatoria+" crossRate: "+this.crossRate);
            if(k%2 != 0 || probabilidadAleatoria < this.crossRate){
                //posición del vector ruidoso
                trial[k] = vectorRuidoso[k];
            }
        }
        
        //se repara el trial considerando las restricciones de una sola variable dada la naturaleza del problema. Si sale de los límites de las restricciones, entonces se remplaza el valor de la variable por el límite superior o inferior según correponda
        var restriccion = this.restricciones[0];
        for(var i=0; i<trial.length; i++){
            if(restriccion[0] > trial[i] || restriccion[1] < trial[i]){
                trial[i] = this.getNumeroAleatorioEnRango(restriccion[0], restriccion[1]);
                this.reparaciones++;
            }
            
            /*if(restriccion[0] > trial[i] || restriccion[1] < trial[i]){
                trial[i] = restriccion[0];
                this.reparaciones++;
            }else if(restriccion[1] < trial[i]){
                trial[i] = restriccion[1];
                this.reparaciones++;
            }*/
        }
        
        //console.log("trial: "+trial);
        var nuevoIndividuo = new Individuo();
        nuevoIndividuo.setGenotipo(trial);
        nuevoIndividuo.setAptitud(this.calculaAptitudDeIndividuo(nuevoIndividuo));
        return nuevoIndividuo;
    }
    
    this.getSumaDeVectores = function(vectorUno, vectorDos){
        var nuevoVector = new Array();
        
        for(var i=0; i<vectorUno.length; i++){
            nuevoVector.push(vectorUno[i] + vectorDos[i]);
        }
        
        return nuevoVector.slice(0);
    }
    
    this.getMultiplicacionDeVectorPorEscalar = function(vectorUno, escalar){
        //console.log("vectorBase: "+vectorUno+" escalar: "+escalar);
        var nuevoVector = new Array();
        
        for(var i=0; i<vectorUno.length; i++){
            nuevoVector.push(vectorUno[i] * escalar);
        }
        
        return nuevoVector.slice(0);
    }
    
    this.getRestaDeVectores = function(vectorUno, vectorDos){
        var nuevoVector = new Array();
        
        for(var i=0; i<vectorUno.length; i++){
            nuevoVector.push(vectorUno[i] - vectorDos[i]);
        }
        
        return nuevoVector.slice(0);
    }
    
    this.generaIndividuoAleatorio = function(individuoPrevio){
        var nuevoGenotipo = new Array();
        if(individuoPrevio) nuevoGenotipo = individuoPrevio.getGenotipo();
        
        var numeroAleatorio = 0.0;
        var restriccion = null;
        var coordenadaActual = 1;
        for(var i=this.posicionesProtegidas; i<this.restricciones.length; i++){
            if(i % 2 == 0){
                nuevoGenotipo.push(parseInt(coordenadaActual));
                coordenadaActual++;
            }else{
                restriccion = this.restricciones[i];
                numeroAleatorio = this.getNumeroAleatorioEnRango(restriccion[0], restriccion[1]);
                while(numeroAleatorio < restriccion[0] || numeroAleatorio > restriccion[1]){
                    numeroAleatorio = this.getNumeroAleatorioEnRango(restriccion[0], restriccion[1]);
                }
                nuevoGenotipo.push(parseInt(numeroAleatorio));   
            }
        }
        var nuevoIndividuo = new Individuo();
        nuevoIndividuo.setGenotipo(nuevoGenotipo);
        return nuevoIndividuo;
    }
    
    this.getNumeroAleatorioEnRango = function(inicio, fin){
        return parseFloat((Math.random() * fin) + inicio);
    }
    
    this.calculaAptitudDeIndividuo = function(individuo){
        //la aptitud del individuo se calcula
        //aptitud = numeroReynasAtacadas * 10;
        individuo = individuo.getGenotipo();
        var numeroReynasAtacadas = 0;
        var suma; var resta; var suma2; var resta2;
        //console.log("aptitud de individuo: "+individuo);
        for(var i=0; i<individuo.length-2; i += 2){
            suma = (individuo[i]+individuo[i+1]);
            resta = (individuo[i]-individuo[i+1]);
            
            for(var k=i+2; k<individuo.length; k+=2){
                suma2 = (individuo[k]+individuo[k+1]);
                resta2 = (individuo[k]-individuo[k+1]);
                
                /*console.log("x: "+individuo[i]+" y: "+individuo[i+1]);
                console.log("x2: "+individuo[k]+" y2: "+individuo[k+1]);
                console.log("suma: "+suma+" suma2: "+suma2);
                console.log("resta: "+resta+" resta: "+resta2);*/
                
                if(suma == suma2 || resta == resta2 || individuo[i] == individuo[k] || individuo[i+1] == individuo[k+1]) numeroReynasAtacadas++;
            }
        }
        //console.log("reynas atacadas:"+numeroReynasAtacadas);
        return numeroReynasAtacadas;
    }
    
    this.getMejorIndividuoDePoblacion = function(){
        var mejorAptitud = 100000000;
        var posicion = -1;
        for(var i=0; i<this.individuos.length; i++){
            if(this.individuos[i].getAptitud() < mejorAptitud){
                posicion = i;
                mejorAptitud = this.individuos[i].getAptitud();
            }
        }
        return this.individuos[posicion];
    }
}