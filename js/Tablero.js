var Tablero = function(){
    
    this.contenedor = null;
    this.nombreInstancia = null;
    this.numeroCuadros = 8;
    this.reinas = null;
    
    this.setReinas = function(reinas){
        this.reinas = reinas;
    }
    
    this.setComponente = function(contenedor, nombreInstancia){
        this.contenedor = contenedor;
        this.nombreInstancia = nombreInstancia;
    }
    
    this.setNumeroDeCuadros = function(numero){
        if(numero < 8) return;
        if(numero % 2 != 0){
            console.log("Debe ser un número par");
            return;
        }
        
        this.numeroCuadros = numero;
    }
    
    this.muestraComponente = function(){    
        var alto = getAlto();
        var contenido = "<table align='center' width='"+alto+"px' cellpadding='0' cellspacing='3'>";
        var porcentaje = 100/this.numeroCuadros;
        var banderaColor = false;
        var altoCelda = alto / this.numeroCuadros;
        
        for(var i=0; i<this.numeroCuadros; i++){
            banderaColor = !banderaColor;
            contenido += "<tr>";
            for(var j=0; j<this.numeroCuadros; j++){
                if(banderaColor){
                    color = "background-color:#000000;";
                    banderaColor = !banderaColor;
                }else{
                    color = "background-color:#ffffff;";
                    banderaColor = !banderaColor;
                }
                
                contenido += "<td width='"+porcentaje+"%' height='"+altoCelda+"px' style='"+color+"' id='"+(i+1)+"_"+(j+1)+"'></td>";
            }    
            contenido += "</tr>";
        }
        
        contenido += "</table>";
        this.contenedor.innerHTML = contenido;
        
        //agregamos las reinas en el tablero
        var casilla = null; var reina = null;
        for(var i=0; i<this.reinas.length; i++){
            reina = this.reinas[i];
            casilla = document.getElementById(reina[0]+"_"+reina[1]);
            if(casilla) casilla.style = "background-color:#0000ff;";
        }
    }
}