var AppBusquedaPixeles = function(){
    
    this.nombreInstancia = "";
    this.contenedor = null;
    this.selectorPeriodo = null;
    this.formaBusquedaIndividual = null;
    this.formaBusquedaPorArchivo = null;
    
    this.setComponente = function(nombreInstancia, contenedor){
        this.nombreInstancia = nombreInstancia;
        this.contenedor = contenedor;
    }
    
    this.procesaSesionCerrada = function(respuesta){
        window.location = window.location;
    }
    
    this.cerrarSesion = function(){
        getResultadoAjax("opcion=6", this.nombreInstancia+".procesaSesionCerrada", document.getElementById("zona_cerrar_sesion"));
    }
    
    this.muestraComponente = function(){
        var contenido = "<br/><div class='right-align'><label id='user' style='padding-right:12px; font-size:18px;'></label><a class='waves-effect waves-light btn' style='background-color:#009ae1;' onclick='"+this.nombreInstancia+".cerrarSesion()'>Salir</a></div>";
        contenido += '<div class="card" style="padding:15px;">';
        contenido += '<h3 style="color:#009ae1;">Búsqueda de Pixeles</h3>';
        contenido += '<div class="row">';
        contenido += '<div class="col s12">';
        
        contenido += '<br/><div id="'+this.nombreInstancia+'_selectorPeriodo"></div><br/>';
        
        contenido += '<ul id="tabs-swipe-demo" class="tabs">';
        contenido += '<li class="tab col s4"><a class="active" href="#test-swipe-1" style="color:#009ae1;">Individual</a></li>';
        contenido += '<li class="tab col s4"><a href="#test-swipe-2" style="color:#009ae1;">Archivo</a></li>';
        contenido += '</ul>';
        contenido += '<div id="test-swipe-1" class="col s12"></div>';
        contenido += '<div id="test-swipe-2" class="col s12">Test 2</div>';
        
        contenido += '</div>';
        contenido += '</div>';
        contenido += '</div>';
        
        this.contenedor.setAttribute("class", "container");
        this.contenedor.innerHTML = contenido;
        
        $(document).ready(function(){
            $('ul.tabs').tabs();
        });
        
        this.selectorPeriodo = new SelectorDePeriodo();
        this.selectorPeriodo.setComponente(this.nombreInstancia+".selectorPeriodo", document.getElementById(this.nombreInstancia+"_selectorPeriodo"));
        this.selectorPeriodo.muestraComponente();
        
        this.formaBusquedaIndividual = new FormaBusquedaIndividualPixeles();
        this.formaBusquedaIndividual.setComponente(this.nombreInstancia+".formaBusquedaIndividual", document.getElementById("test-swipe-1"));
        this.formaBusquedaIndividual.muestraComponente();
        
        this.formaBusquedaPorArchivo = new FormaBusquedaPixelesConArchivo();
        this.formaBusquedaPorArchivo.setComponente(this.nombreInstancia+".formaBusquedaPorArchivo", document.getElementById("test-swipe-2"));
        this.formaBusquedaPorArchivo.muestraComponente();
        
        document.getElementById("user").innerHTML = "<b>"+login.getNombreUserLogueado()+"</b>";
    }
}