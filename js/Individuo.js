var Individuo = function(){
    
    this.genotipo = null;
    this.aptitud = 100.0;
    
    this.setGenotipo = function(genotipo){
        //se repara redondeando todos los valores a entero
        this.genotipo = new Array();
        for(var i=0; i<genotipo.length; i++) this.genotipo[i] = parseInt(genotipo[i]);
    }
    
    this.getGenotipo = function(){
        return this.genotipo;
    }
    
    this.getAptitud = function(){
        return this.aptitud;
    }
    
    this.setAptitud = function(aptitud){
        this.aptitud = aptitud;
    }
}