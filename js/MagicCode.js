function objetoAjax()
{
    if (window.XMLHttpRequest){
        // code for IE7+, Firefox, Chrome, Opera, Safari
        objeto_ajax = new XMLHttpRequest();
		return objeto_ajax;
    }
    if (window.ActiveXObject){
        // code for IE6, IE5
        objeto_ajax = new ActiveXObject("Microsoft.XMLHTTP");
    	return objeto_ajax;
    }
    return null;
}

var enlace = document.location+"php/EnlaceBuscador.php";
var app = null;
var login = null;

function sortMatrix(matrix, dataPosition){
    if(matrix.lenth < 2) return matrix;//if there is no more than 2 vectors in the matrix, we return the same matrix
    var sortedMatrix = new Array();
    var currentVector; var secondVector; var position;
    while(matrix.length > 1){
        currentVector = matrix[0];
        position = 0;
        for(var i=1; i < matrix.length; i++){
            secondVector = matrix[i];
            if(currentVector[dataPosition] >= secondVector[dataPosition]){
                position = i;
                currentVector = secondVector;
            }
        }
        sortedMatrix.push(currentVector);
        
        var aux = new Array();
        for(var i=0; i < matrix.length; i++){
            if(i != position) aux.push(matrix[i]);
        }
        matrix = aux.slice();
    }
    sortedMatrix.push(matrix[0]);
    
    return sortedMatrix.slice();
}

function iniciaApp(){
    app = new AppBusquedaPixeles();
    app.setComponente("app", document.getElementById('contenedorPrincipal'));
    app.muestraComponente();
}

function getResultadoAjaxNS(opciones, funcion, contenedor){
    //console.log("opciones: "+opciones);
    if(contenedor) contenedor.innerHTML = "<img src='imagenes/ajax-loader-gd.gif'/>";
    var ajax = objetoAjax();
    ajax.open("POST", enlace, true);
    ajax.onreadystatechange=function() {
	   if (ajax.readyState == 4){
           respuestas_servidor[posicion_respuestas] = ajax.responseText;
           setTimeout(funcion+"(getRespuestaServidor("+posicion_respuestas+"))", 0);
           posicion_respuestas++;
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send(opciones);
}

function getResultadoAjax(opciones, funcion, contenedor){
    if(tipo_ajax != 1) {
        getResultadoAjaxNS(opciones, funcion, contenedor);
        return;
    }
    
    if(contenedor) contenedor.innerHTML = "<img src='imagenes/ajax-loader-gd.gif'/>";
    var ajax = objetoAjax();
    ajax.open("POST", enlace, true);
    ajax.onreadystatechange=function() {
	   if (ajax.readyState == 4){
           var respuesta = ajax.responseText;
           if(respuesta == 1) getServerResponse(opciones, funcion);
           else{
               login.muestraFormaLogueo();
           }
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("token="+((login)?login.getToken():"")+"&opcion=0");
}

var tipo_ajax = 1;

function getServerResponse(opciones, funcion){
    var ajax = objetoAjax();
    ajax.open("POST", enlace, true);
    ajax.onreadystatechange=function() {
	   if (ajax.readyState == 4){
           respuestas_servidor[posicion_respuestas] = ajax.responseText;
           setTimeout(funcion+"(getRespuestaServidor("+posicion_respuestas+"))", 0);
           posicion_respuestas++;
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("token="+((login)?login.getToken():"")+"&"+opciones);
}

var respuestas_servidor = new Array();
var posicion_respuestas = 0;

function getRespuestaServidor(posicion){
    var respuesta = respuestas_servidor[posicion];
    respuestas_servidor[posicion] = "";
    return respuesta;
}

function limpiaContenido(contenido_inicial, campo){
	var con = campo.value;
	if( contenido_inicial == con) campo.value = "";
}

function llenaContenido(contenido_inicial, campo){
	var con = campo.value;
	if( con == "") campo.value = contenido_inicial;
}

function getAlto(){
	return jQuery(window).height();
}

function getAncho(){
	return jQuery(window).width();
}

var tablero = null;
var evolucionDiferencial = null;
function cargaApp(){
    var restricciones = new Array();
    
    var numeroReynasIniciales = 4;
    var numeroReynasTotales = 16;
    
    var restriccion = new Array();
    restriccion[0] = 1;
    restriccion[1] = parseInt(numeroReynasTotales/2);
    
    //restrincciones para las reynas iniciales
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    
    //invocamos a evolución diferencial considerando solo las reynas iniciales
    this.evolucionDiferencial = new EvolucionDiferencial();
    var posicionesProtegidas = 0;
    for(var i=numeroReynasIniciales; i<=numeroReynasTotales; i+=2){
        //cr, mr
        this.evolucionDiferencial.inicializaEvolucionDiferencial(10, 10*i, 0.5, 0.1, restricciones.slice(0), posicionesProtegidas);
        var mejorSolucion = this.evolucionDiferencial.getMejorIndividuo();
        var individuos = this.evolucionDiferencial.getIndividuos();
        console.log(individuos);
        var casillas = new Array();
        for(var i=0; i<mejorSolucion.length; i+=2){
            casillas.push([mejorSolucion[i], mejorSolucion[i+1]]);
        }
        console.log("casillas: "+casillas);

        tablero = new Tablero();
        tablero.setComponente(document.getElementById("contenedorPrincipal"), "tablero");
        tablero.setReinas(casillas);
        tablero.muestraComponente();
        
        restricciones.push(restriccion);
        restricciones.push(restriccion);
        
        if(posicionesProtegidas == 0) posicionesProtegidas += 4;
        else posicionesProtegidas += 2;
        
        console.log("i: "+i);
        if(i > 4) break;
    }
    /*
    
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    restricciones.push(restriccion);
    */
    
}